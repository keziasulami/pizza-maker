# Pizza Maker

Pizza Maker adalah permainan membuat dan menghias pizza yang berbasis website. Pizza Maker ditujukan untuk pengguna anak-anak.

## Pipeline and Coverage

|branch|pipeline|coverage|
|------|--------|--------|
|master|[![pipeline status](https://gitlab.com/zafirabinta/pizza-maker/badges/master/pipeline.svg)](https://gitlab.com/zafirabinta/pizza-maker/-/commits/master)|[![coverage report](https://gitlab.com/zafirabinta/pizza-maker/badges/master/coverage.svg)](https://gitlab.com/zafirabinta/pizza-maker/-/commits/master)|
|aan-scoreboard|[![pipeline status](https://gitlab.com/zafirabinta/pizza-maker/badges/aan-scoreboard-new/pipeline.svg)](https://gitlab.com/zafirabinta/pizza-maker/-/commits/aan-scoreboard-new)|[![coverage report](https://gitlab.com/zafirabinta/pizza-maker/badges/aan-scoreboard-new/coverage.svg)](https://gitlab.com/zafirabinta/pizza-maker/-/commits/aan-scoreboard-new)|
|kezia-topping|[![pipeline status](https://gitlab.com/zafirabinta/pizza-maker/badges/kezia-topping/pipeline.svg)](https://gitlab.com/zafirabinta/pizza-maker/-/commits/kezia-topping)|[![coverage report](https://gitlab.com/zafirabinta/pizza-maker/badges/kezia-topping/coverage.svg)](https://gitlab.com/zafirabinta/pizza-maker/-/commits/kezia-topping)|
|level-topping|[![pipeline status](https://gitlab.com/zafirabinta/pizza-maker/badges/level-topping/pipeline.svg)](https://gitlab.com/zafirabinta/pizza-maker/-/commits/level-topping)|[![coverage report](https://gitlab.com/zafirabinta/pizza-maker/badges/level-topping/coverage.svg)](https://gitlab.com/zafirabinta/pizza-maker/-/commits/level-topping)|
|level|[![pipeline status](https://gitlab.com/zafirabinta/pizza-maker/badges/level/pipeline.svg)](https://gitlab.com/zafirabinta/pizza-maker/-/commits/level)|[![coverage report](https://gitlab.com/zafirabinta/pizza-maker/badges/level/coverage.svg)](https://gitlab.com/zafirabinta/pizza-maker/-/commits/level)|
|zafira-auth-2|[![pipeline status](https://gitlab.com/zafirabinta/pizza-maker/badges/zafira-auth-2/pipeline.svg)](https://gitlab.com/zafirabinta/pizza-maker/-/commits/zafira-auth-2)|[![coverage report](https://gitlab.com/zafirabinta/pizza-maker/badges/zafira-auth-2/coverage.svg)](https://gitlab.com/zafirabinta/pizza-maker/-/commits/zafira-auth-2)|

## Authors: Kelompok 14

* [Aan Nur Wahidi](https://gitlab.com/aannurwahidi03)
* [Christian Denata](https://gitlab.com/RagnaChris)
* [Kezia Sulami](https://gitlab.com/keziasulami)
* [Zafira Binta](https://gitlab.com/zafirabinta)

## Credit

Advanced Programming 2019/2020 Genap, Kelas B
