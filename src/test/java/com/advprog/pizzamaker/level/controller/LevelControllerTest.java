package com.advprog.pizzamaker.level.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.advprog.pizzamaker.controller.MainController;
import com.advprog.pizzamaker.level.service.LevelService;
import com.advprog.pizzamaker.scoreboard.controller.StandaloneMvcTestViewResolver;
import com.advprog.pizzamaker.scoreboard.service.ScoreboardServiceImp;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.web.method.annotation.AuthenticationPrincipalArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringRunner.class)
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.MOCK,
    classes = LevelController.class)
@AutoConfigureMockMvc
public class LevelControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LevelService levelService;

    @MockBean
    private ScoreboardServiceImp scoreboardServiceImp;

    /**
     * Setting Up for handling exceptions.
     */
    @Before
    public void setUp() {
        final LevelController controller = new LevelController(scoreboardServiceImp, levelService);

        mockMvc =
                MockMvcBuilders.standaloneSetup(controller)
                        .setViewResolvers(new StandaloneMvcTestViewResolver())
                        .setCustomArgumentResolvers(new AuthenticationPrincipalArgumentResolver())
                        .build();
    }

    @Test
    @WithMockUser
    public void whenMakePizzaURLIsAccessedItShouldCallLevelServiceEditPizza() throws Exception {

        mockMvc.perform(get("/make-pizza"))
            .andExpect(handler().methodName("makePizza"));
    }

    @Test
    @WithMockUser
    public void whenAddToppingURLIsAccessedItShouldCallLevelServiceEditPizza() throws Exception {

        mockMvc.perform(post("/add-topping")
            .contentType("application/json")
            .param("toppingPizza", "cheese")
            .with(csrf()))
            .andExpect(handler().methodName("addTopping"));
    }

    // @Test
    // public void whenSubmitURLIsAccessedItShouldCallLevelServiceSubmit() throws Exception {

    //     mockMvc.perform(post("/submit")
    //         .contentType("application/json")
    //         .param("time", "10")
    //         .with(csrf()))
    //         .andExpect(status().isOk());
    // }

    @Test
    @WithMockUser
    public void whenTrashURLIsAccessedItShouldCallLevelServiceTrash() throws Exception {

        mockMvc.perform(get("/trash"))
            .andExpect(handler().methodName("trash"));
    }

    @Test
    @WithMockUser
    public void whenNextLevelURLIsAccessedItShouldCallLevelServiceNextLevel() throws Exception {

        mockMvc.perform(get("/next-level"))
            .andExpect(handler().methodName("nextLevel"));
    }

    @Test
    @WithMockUser
    public void whenRemoveOrderURLIsAccessedItShouldCallLevelServiceRemoveOrder() throws Exception {

        mockMvc.perform(get("/remove-order"))
            .andExpect(handler().methodName("removeOrder"));
    }
}

