package com.advprog.pizzamaker.level.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.advprog.pizzamaker.level.core.buttons.Button;
import com.advprog.pizzamaker.level.core.buttons.PepperoniButton;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PepperoniButtonTest {

    private Button pepperoniButton;
    private Level level;

    @BeforeEach
    public void setUp() throws Exception {
        level = new Level();
        pepperoniButton = new PepperoniButton(level);
    }

    @Test
    public void testPepperoniButtonName() {
        assertEquals("pepperoni", pepperoniButton.getName());
    }

    @Test
    public void testAddPepperonitoPizza() {
        level.createPizza();
        pepperoniButton.execute();
        assertEquals(1, level.getPizza().getPepperoni());
    }

    @Test
    public void testSubmitNotWorking() {
        assertEquals(0, pepperoniButton.submit(10));
    }

    @Test
    public void testPenaltyNotWorking() {
        assertEquals(0, pepperoniButton.penalty());
    }
}