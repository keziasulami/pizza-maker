package com.advprog.pizzamaker.level.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.advprog.pizzamaker.level.core.buttons.Button;
import com.advprog.pizzamaker.level.core.buttons.MushroomButton;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MushroomButtonTest {

    private Button mushroomButton;
    private Level level;

    @BeforeEach
    public void setUp() throws Exception {
        level = new Level();
        mushroomButton = new MushroomButton(level);
    }

    @Test
    public void testMushroomButtonName() {
        assertEquals("mushroom", mushroomButton.getName());
    }

    @Test
    public void testAddMushroomtoPizza() {
        level.createPizza();
        mushroomButton.execute();
        assertEquals(1, level.getPizza().getMushroom());
    }

    @Test
    public void testSubmitNotWorking() {
        assertEquals(0, mushroomButton.submit(10));
    }

    @Test
    public void testPenaltyNotWorking() {
        assertEquals(0, mushroomButton.penalty());
    }
}