package com.advprog.pizzamaker.level.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.advprog.pizzamaker.level.core.Level;
import com.advprog.pizzamaker.level.core.Order;
import com.advprog.pizzamaker.level.core.buttons.Button;
import com.advprog.pizzamaker.level.core.buttons.CreatePizzaButton;
import com.advprog.pizzamaker.level.repository.ButtonRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class LevelServiceImplTest {

    @Mock
    private ButtonRepository buttonRepo;

    @Mock
    private Level level;

    @InjectMocks
    private LevelServiceImpl levelService;

    @Test
    public void whenNextLevelIsCalledItShouldCallLevelNextLevel() {
        Level level = levelService.getLevel();
        int response = levelService.nextLevel();
        assertEquals(-1, response);
        assertEquals(1, levelService.getLevelDifficulty());
    }

    @Test
    public void whenEditPizzaIsCalledItShouldCallButtonRepositoryEditPizza() {
        Button button = new CreatePizzaButton(level);

        levelService.editPizza(button.getName());

        verify(buttonRepo, times(1)).executeButton(button.getName());
    }

    @Test
    public void whenGetButtonsIsCalledItShouldCallButtonRepositoryoGetButtons() {
        levelService.getButtons();

        verify(buttonRepo, atLeastOnce()).getButtons();
    }

    @Test
    public void whenSubmitCalledShouldCallButtonRepositorySubmit() throws JsonProcessingException {
        levelService.editPizza("create pizza");
        levelService.createOrder();
        levelService.submit(10);

        verify(buttonRepo, times(1)).submit(10);
    }

    @Test
    public void whenTrashIsCalledItShouldCallButtonRepositoryTrash() {
        levelService.editPizza("create pizza");
        levelService.trash();

        verify(buttonRepo, times(1)).trash();
    }

    @Test
    public void whenGetLevelDifficultyIsCalledItShouldCallLevelGetLevel() {
        Level level = levelService.getLevel();
        int serviceLevel = levelService.getLevelDifficulty();
        int levelLevel = level.getLevel();

        assertEquals(levelLevel, serviceLevel);
    }

    @Test
    public void whenRemoveOrderShouldCallLevelRemoveOrder() throws JsonProcessingException {
        Level level = levelService.getLevel();
        levelService.createOrder();
        levelService.removeOrder();

        assertEquals(null, level.getFirstOrder());
    }
}