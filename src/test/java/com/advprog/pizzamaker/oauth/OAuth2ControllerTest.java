package com.advprog.pizzamaker.oauth;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.advprog.pizzamaker.controller.MainController;
import com.advprog.pizzamaker.scoreboard.controller.StandaloneMvcTestViewResolver;
import com.advprog.pizzamaker.scoreboard.service.ScoreboardServiceImp;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.oauth2.core.user.OAuth2UserAuthority;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.web.method.annotation.AuthenticationPrincipalArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = MainController.class)
@AutoConfigureMockMvc
public class OAuth2ControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ScoreboardServiceImp scoreboardServiceImp;

    /**
     * Setting Up for handling exceptions.
     */
    @Before
    public void setUp() {
        final MainController controller = new MainController(scoreboardServiceImp);
        mockMvc =
                MockMvcBuilders.standaloneSetup(controller)
                        .setViewResolvers(new StandaloneMvcTestViewResolver())
                        .setCustomArgumentResolvers(new AuthenticationPrincipalArgumentResolver())
                        .build();
    }

    /**
     * create Oauth2User.
     */
    public static OAuth2User createOAuth2User(String name, String email) {
        Map<String, Object> authorityAttributes = new HashMap<>();
        authorityAttributes.put("key", "value");
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("sub", "1234567890");
        attributes.put("name", name);
        attributes.put("email", email);
        GrantedAuthority authority = new OAuth2UserAuthority(authorityAttributes);

        return new DefaultOAuth2User(asList(authority), attributes, "sub");
    }

    /**
     * get Oauth Authentication.
     */
    public static Authentication getOauthAuthenticationFor(OAuth2User user) {
        Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
        String authorizedClientRegistrationId = "my-oauth-client";

        return new OAuth2AuthenticationToken(user, authorities, authorizedClientRegistrationId);
    }

    /**
     *without login Test.
     */
    @Test
    @WithMockUser
    public void landingPageSuccessfullyAccessedWithoutLogin() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void navbarSuccessfullyAccessed() throws Exception {
        mockMvc.perform(get("/navbar"))
                .andExpect(status().isOk());
    }

    @Test
    public void landingPageSuccessfullyAccessedWithLogin() throws Exception {
        OAuth2User principal = createOAuth2User("user", "user@gmail.com");
        mockMvc.perform(MockMvcRequestBuilders.get("/")
                .with(authentication(getOauthAuthenticationFor(principal))))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("")));
    }
}
