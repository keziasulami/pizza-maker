package com.advprog.pizzamaker.pizzadecorator.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.advprog.pizzamaker.pizzadecorator.service.PizzaService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.MOCK,
    classes = PizzaController.class)
@AutoConfigureMockMvc
public class PizzaControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PizzaService pizzaService;

    @Test
    @WithMockUser
    public void whenPizzaUrlWithoutIdIsAccessedItShouldRedirect() throws Exception {

        mockMvc.perform(get("/pizza"))
            .andExpect(status().is3xxRedirection())
            .andExpect(redirectedUrl("/pizza/0"));
    }

    @Test
    @WithMockUser
    public void whenPizzaUrlWithWrongIdIsAccessedItShouldReturnErrorPage() throws Exception {

        mockMvc.perform(get("/pizza/100"))
            .andExpect(status().isOk())
            .andExpect(view().name("pizza-exceeded"));
    }

}
