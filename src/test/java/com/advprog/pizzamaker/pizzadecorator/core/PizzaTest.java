package com.advprog.pizzamaker.pizzadecorator.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class PizzaTest {

    private Pizza pizza;
    private Pizza mockPizza;

    @BeforeEach
    public void setUp() {
        pizza = new Pizza();
        mockPizza = Mockito.mock(Pizza.class);
    }

    @Test
    public void testMethodGetPepperoni() {
        assertEquals(pizza.getPepperoni(), 0);

        Mockito.doCallRealMethod().when(mockPizza).getPepperoni();
        Mockito.when(mockPizza.getPepperoni()).thenReturn(0);
        int pepperoni = mockPizza.getPepperoni();

        Mockito.verify(mockPizza, Mockito.times(1)).getPepperoni();
        assertEquals(0, pepperoni);
    }

    @Test
    public void testMethodGetMushroom() {
        assertEquals(pizza.getMushroom(), 0);

        Mockito.doCallRealMethod().when(mockPizza).getMushroom();
        Mockito.when(mockPizza.getMushroom()).thenReturn(0);
        int mushroom = mockPizza.getMushroom();

        Mockito.verify(mockPizza, Mockito.times(1)).getMushroom();
        assertEquals(0, mushroom);
    }

    @Test
    public void testMethodGetCheese() {
        assertEquals(pizza.getCheese(), 0);

        Mockito.doCallRealMethod().when(mockPizza).getCheese();
        Mockito.when(mockPizza.getCheese()).thenReturn(0);
        int cheese = mockPizza.getCheese();

        Mockito.verify(mockPizza, Mockito.times(1)).getCheese();
        assertEquals(0, cheese);
    }

}
