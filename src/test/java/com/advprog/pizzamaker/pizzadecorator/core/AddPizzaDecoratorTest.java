package com.advprog.pizzamaker.pizzadecorator.core;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class AddPizzaDecoratorTest {

    Pizza pepperoni;
    Pizza mushroom;
    Pizza cheese;

    @Test
    public void testAddPizzaDecorator() {

        pepperoni = AddPizzaDecorator.PEPPERONI.add(pepperoni);
        mushroom = AddPizzaDecorator.MUSHROOM.add(mushroom);
        cheese = AddPizzaDecorator.CHEESE.add(cheese);

        assertTrue(pepperoni instanceof Pepperoni);
        assertTrue(mushroom instanceof Mushroom);
        assertTrue(cheese instanceof Cheese);

    }

}
