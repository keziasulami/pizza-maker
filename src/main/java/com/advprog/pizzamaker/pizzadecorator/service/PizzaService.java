package com.advprog.pizzamaker.pizzadecorator.service;

import com.advprog.pizzamaker.pizzadecorator.core.Pizza;
import com.advprog.pizzamaker.pizzadecorator.core.PizzaTopping;

public interface PizzaService {

    Pizza getPizzaById(int id) throws Exception;

    int getNewId();

    void addTopping(PizzaTopping pizzaTopping);

}
