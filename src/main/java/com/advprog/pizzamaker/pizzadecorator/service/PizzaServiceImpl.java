package com.advprog.pizzamaker.pizzadecorator.service;

import com.advprog.pizzamaker.pizzadecorator.core.AddPizzaDecorator;
import com.advprog.pizzamaker.pizzadecorator.core.Pizza;
import com.advprog.pizzamaker.pizzadecorator.core.PizzaTopping;
import com.advprog.pizzamaker.pizzadecorator.repository.PizzaRepository;
import org.springframework.stereotype.Service;

@Service
public class PizzaServiceImpl implements PizzaService {

    private PizzaRepository pizzaRepository;

    public PizzaServiceImpl(PizzaRepository pizzaRepository) {
        this.pizzaRepository = pizzaRepository;
    }

    @Override
    public Pizza getPizzaById(int id) throws Exception {
        if (id > getNewId()) {
            throw new Exception();
        } else {
            int pepperoni = pizzaRepository.getPepperoniById(id);
            int mushroom = pizzaRepository.getMushroomById(id);
            int cheese = pizzaRepository.getCheeseById(id);

            Pizza pizza = new Pizza();
            while (pepperoni-- > 0) {
                pizza = AddPizzaDecorator.PEPPERONI.add(pizza);
            }
            while (mushroom-- > 0) {
                pizza = AddPizzaDecorator.MUSHROOM.add(pizza);
            }
            while (cheese-- > 0) {
                pizza = AddPizzaDecorator.CHEESE.add(pizza);
            }
            return pizza;
        }
    }

    @Override
    public int getNewId() {
        return pizzaRepository.getNumberofPizza();
    }

    @Override
    public void addTopping(PizzaTopping pizzaTopping) {
        pizzaRepository.save(pizzaTopping);
    }

}
