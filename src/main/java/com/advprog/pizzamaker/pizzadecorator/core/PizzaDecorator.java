package com.advprog.pizzamaker.pizzadecorator.core;

abstract class PizzaDecorator extends Pizza {
    Pizza pizza;

    @Override
    public int getPepperoni() {
        return this.pizza.getPepperoni();
    }

    @Override
    public int getMushroom() {
        return this.pizza.getMushroom();
    }

    @Override
    public int getCheese() {
        return this.pizza.getCheese();
    }

}
