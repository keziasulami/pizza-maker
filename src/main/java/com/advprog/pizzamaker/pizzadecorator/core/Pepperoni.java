package com.advprog.pizzamaker.pizzadecorator.core;

public class Pepperoni extends PizzaDecorator {

    public Pepperoni(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public int getPepperoni() {
        return this.pizza.getPepperoni() + 1;
    }

}
