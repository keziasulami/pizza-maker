package com.advprog.pizzamaker.pizzadecorator.core;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PizzaTopping")
public class PizzaTopping {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "pizzaId")
    private int pizzaId;

    @Column(name = "toppingType")
    private String toppingType;

    public PizzaTopping(int pizzaId, String toppingType) {
        this.pizzaId = pizzaId;
        this.toppingType = toppingType;
    }
}
