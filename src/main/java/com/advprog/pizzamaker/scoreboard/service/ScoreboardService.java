package com.advprog.pizzamaker.scoreboard.service;

import com.advprog.pizzamaker.scoreboard.core.Player;
import com.advprog.pizzamaker.scoreboard.repository.PlayerRepository;
import java.util.List;
import java.util.Optional;

public interface ScoreboardService {

    List<Player> findAll();

    List<Player> getPlayers();

    Optional<Player> findByEmail(String email);

    Player updateScoreLevel(String email, int score, int level);

    void updatePlayer();

    void addPlayer(Player player);

    PlayerRepository getRepository();
}
