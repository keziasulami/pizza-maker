package com.advprog.pizzamaker.scoreboard.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TopScore {

    public List<Player> listOfScore = new ArrayList<>();

    /**
     * Notify all DataPlayer.
     */
    public void broadcast() {
        update(listOfScore, listOfScore.size());
        for (Player data : listOfScore) {
            data.update();
        }
    }

    /**
     * Method if listOfScore contains the same Email.
     * @return boolean
     */
    public boolean containsEmail(Player player) {
        for (Player players : listOfScore) {
            if (players.getEmail().equals(player.getEmail())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Add player to listOfScore.
     */
    public void addTopScore(Player player) {
        if (!(containsEmail(player))) {
            this.listOfScore.add(player);
            player.setRank(indexOf(player) + 1);
            broadcast();
        }
    }

    /**
     * Rewriting player.
     */
    public void rewritePlayer(Player player) {
        this.deletePlayer(player);
        this.listOfScore.add(player);
        player.setRank(indexOf(player) + 1);
        broadcast();
    }

    /**
     * Deleting player.
     */
    public void deletePlayer(Player player) {
        for (int i = 0; i < listOfScore.size(); i++) {
            Player players = listOfScore.get(i);
            if (players.getEmail().equals(player.getEmail())) {
                listOfScore.remove(players);
            }
        }
    }

    /**
     * updating data of player.
     */
    public void update(List<Player> list, int size) {
        if (size <= 1) {
            return;
        }

        update(list, size - 1);
        Player last = list.get(size - 1);
        int beforeLast = size - 2;

        while (beforeLast >= 0 && list.get(beforeLast).getScore() < last.getScore()) {
            list.set(beforeLast + 1, list.get(beforeLast));
            beforeLast--;
        }

        list.set(beforeLast + 1, last);
    }

    /**
     * check index of the object in listOfScore.
     */
    public int indexOf(Player player) {
        for (int i = 0; i < listOfScore.size(); i++) {
            if (listOfScore.get(i).getEmail().equals(player.getEmail())) {
                return i;
            }
        }
        return -1;
    }
}
