package com.advprog.pizzamaker.level.core;

import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Order {
    private Map<String, String> quest = new HashMap<>();
    private List<String> toppings;

    public Order(List<String> toppings) {
        this.toppings = new ArrayList<>(toppings);
    }

    /**
     * This object create map with ingredients and each quantity.
     */
    public void makeOrder() {
        Random random = new Random();
        int toppingsTotal = 8;
        while (toppingsTotal > 0) {
            if (toppings.size() == 1) {
                this.quest.put(toppings.remove(0), Integer.toString(toppingsTotal));
                break;
            }
            int topping = random.nextInt(toppings.size() - 1);
            int quantity = random.nextInt(toppingsTotal);
            this.quest.put(toppings.remove(topping), Integer.toString(quantity));
            toppingsTotal -= quantity;
        }
    }

    /**
     * Check if the input match the with the order.
     */
    public boolean getTopping(String topping, int quantity) {
        if (quest.get(topping) == null) {
            if (quantity == 0) {
                return true;
            } else {
                return false;
            }
        }

        if (Integer.parseInt(quest.get(topping)) == quantity) {
            return true;
        }

        return false;
    }

    /**
     * Convert Map to JSON so it can be displayed on web.
     */
    public String getOrderJson() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonResult = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this.quest);
        return jsonResult;
    }

    /**
     * Make a test order to test matchPizza() method
     * in SubmitButton works.
     */
    public void testOrder() { 
        int toppingsTotal = 0;
        while (toppingsTotal < 3) {
            this.quest.put(toppings.remove(0), "1");
            toppingsTotal++;
        }
    }
}