package com.advprog.pizzamaker.level.core.buttons;

import com.advprog.pizzamaker.level.core.Level;

public class CreatePizzaButton extends Button {
    private String name = "create pizza";

    public CreatePizzaButton(Level level) {
        super(level);
    }

    public void execute() {
        this.level.createPizza();
    }

    public String getName() {
        return this.name;
    }

    public int submit(int time) {
        return 0;
    }

    public int penalty() {
        return 0;
    }
}