$(function () {
    const token = $("meta[name='_csrf']").attr("content");
    const header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function(e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });
});

$(document).ready(function () {
    $("#createPizza").click( function () {
        displayPizza().then();
    })

    const pizzaId = $("#pizza-id").val();

    $(".topping").click(function (topping) {
        if ($("#pizza-id").val() !== pizzaId) return;
        let top = topping.currentTarget.firstChild.data;
        if (top == 'pepperoni') {
            let pepperoni = parseInt($("#number-pepperoni").val());
            if (addTopping("pepperoni", pepperoni)) {
                pepperoni += 1;
                $("#number-pepperoni").val(pepperoni);
                $("#text-pepperoni").text("Pepperoni: " + pepperoni);

                let json_pepperoni = {
                    pizzaId: pizzaId,
                    toppingType: 'pepperoni'
                };

                $.ajax({
                    method: "POST",
                    url: "/pizza",
                    contentType: "application/json",
                    data: JSON.stringify(json_pepperoni)
                })
            }
        }

        if (top == 'mushroom') {
            let mushroom = parseInt($("#number-mushroom").val());
            if (addTopping("mushroom", mushroom)) {
                mushroom += 1;
                $("#number-mushroom").val(mushroom);
                $("#text-mushroom").text("Mushroom: " + mushroom);

                let json_mushroom = {
                    pizzaId: pizzaId,
                    toppingType: 'mushroom'
                };

                $.ajax({
                    method: "POST",
                    url: "/pizza",
                    contentType: "application/json",
                    data: JSON.stringify(json_mushroom)
                })
            }
        }

        if (top == 'cheese') {
            let cheese = parseInt($("#number-cheese").val());
            if (addTopping("cheese", cheese)) {
                cheese += 1;
                $("#number-cheese").val(cheese);
                $("#text-cheese").text("Cheese: " + cheese);

                let json_cheese = {
                    pizzaId: pizzaId,
                    toppingType: 'cheese'
                };

                $.ajax({
                    method: "POST",
                    url: "/pizza",
                    contentType: "application/json",
                    data: JSON.stringify(json_cheese)
                })
            }
        }
    })

    function loadImage(url) {
        return new Promise(r => { let i = new Image(); i.onload = (() => r(i)); i.src = url; });
    }

    const canvas = document.getElementById("pizzaCanvas");
    const context = canvas.getContext("2d");

    const centerX = canvas.width / 2;
    const centerY = canvas.height / 2;

    const pi = Math.PI;
    const angles = [0, pi, pi * 1.5, pi * .5, pi * 1.75, pi * 1.25, pi * .75, pi * .25];

    let radiuses = {
        "pepperoni": 150,
        "mushroom": 100,
        "cheese": 50
    };

    let images = {};

    async function displayPizza() {
        let radius;
        let i;

        let pizza_image = await loadImage("/images/pizza type-03.png");
        context.drawImage(pizza_image,
            centerX - 450 / 2,
            centerY - 450 / 2,
            450, 450);

        const pepperoni = parseInt($("#number-pepperoni").val());
        images["pepperoni"] = await loadImage("/images/asset-pepperoni.png");
        radius = radiuses["pepperoni"];
        for (i = 0; i < pepperoni; i++) {
            const angle = angles[i];
            context.drawImage(images["pepperoni"],
                centerX + radius * Math.cos(angle) - 50 / 2,
                centerY + radius * Math.sin(angle) - 50 / 2,
                50, 50);
        }

        const mushroom = parseInt($("#number-mushroom").val());
        images["mushroom"] = await loadImage("/images/asset-mushroom.png");
        radius = radiuses["mushroom"];
        for (i = 0; i < mushroom; i++) {
            const angle = angles[i];
            context.drawImage(images["mushroom"],
                centerX + radius * Math.cos(angle) - 50 / 2,
                centerY + radius * Math.sin(angle) - 50 / 2,
                50, 50);
        }

        const cheese = parseInt($("#number-cheese").val());
        images["cheese"] = await loadImage("/images/asset-cheese.png");
        radius = radiuses["cheese"];
        for (i = 0; i < cheese; i++) {
            const angle = angles[i];
            context.drawImage(images["cheese"],
                centerX + radius * Math.cos(angle) - 50 / 2,
                centerY + radius * Math.sin(angle) - 50 / 2,
                50, 50);
        }
    }

    function addTopping(topping_type, number) {
        if (number == 8) {
            $("#maxtopping").show();
        } else {
            let radius = radiuses[topping_type];
            const angle = angles[number];
            context.drawImage(images[topping_type],
                centerX + radius * Math.cos(angle) - 50 / 2,
                centerY + radius * Math.sin(angle) - 50 / 2,
                50, 50);
            return true;
        }
    }
})
