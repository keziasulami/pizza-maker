$(function() {
    const token = $("meta[name='_csrf']").attr("content");
    const header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function(e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });
});

$(document).ready(function () {
    $.ajax({
        url: "/retry-level",
        success: function(result) {
            console.log("Fresh!");
        }
    });

    $('#pizza').load("/pizza");
    var orderId = 0;
    var level = $('#level').text();
    var pizza = 0;
    var topId = 0;
    
    levelTimer();     
    
    function levelTimer() {
        var levelTime = 150 + (30 * level);
        var timer = setInterval(function() {
            levelTime--;
            if (levelTime % 5 == 0) {
                getOrder();
            }
            $('#time').html("time:" + levelTime);
            if (levelTime < 0) {
                clearInterval(timer);
                $('#orders').empty();
                $('#timesup').show();
            }
        }, 1000);
    }

    $('#next').click(function() {
        console.log('next');
        $.ajax({
            url: "/next-level",
            success: function(result) {
                console.log("in");
                console.log(result)
                if (result == 1) {
                    $.ajax({
                        url:"/get-level",
                        success: function(result2) {
                            $('#level').text(result2);
                            $('#score').text(0);
                            level = $('#level').text();
                            levelTimer();
                            $('#timesup').hide();
                        }
                    })
                } else {
                    $('#timesup').hide();
                    $('#notenoughscore').show();
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

    function orderTimer(id) {
        var sec = 15;
        var timer = setInterval(function() {
            sec--;
            $('#order' + id + ' .time').html("<p>time:" + sec + "</p>");
            if (sec < 1) {
                clearInterval(timer);
                removeOrder(id);
            }
        }, 1000);
        return true;
    }

    function removeOrder(id) {
        if ($('#order' + id).length > 0) {
            $.ajax({
                url: "/remove-order",
                success: function(result) {
                    $('#order' + id).remove();
                    topId = id + 1;
                }
            });
        }
    }

    function getOrder() {
        $.ajax({
            url: "/order",
            success: function(result) {
                var id = orderId;
                result = JSON.parse(result);
                $('#orders').append("<div id='order" + id + "' class= 'order'>" + 
                    "Pepperoni: " + result["pepperoni"] +
                    "<br>Mushroom: " + result.mushroom +
                    "<br>Cheese: " + result.cheese +
                    "<p class='time'>time: 15</p></div>");
                orderId++;
                orderTimer(id);
            }
        });
    }

    $('#submit').click(function() {
        // Delete pizza and change score
        let timern = $('#orders div:first p').html();
        if (pizza == 1) {
            $('#pizza').html("");
            removejscssfile("/js/pizza.js", "js");
            $('#pizza').load("/pizza");
            pizza = 0;
            $.ajax({
                type: 'POST',
                url: "/submit",
                data: timern.replace(/[^0-9]/g, ''),
                success: function(result) {
                    let total = parseInt($('#score').text()) + result;
                    $('#score').text(total);
                    if (result > 0) {
                        removeOrder(topId);
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        } else {
            $('#nosubmit').show();
        }
    });

    $('#trash').click(function() {
        // Delete pizza and change score
        if (pizza == 1) {
            $('#pizza').html("");
            removejscssfile("/js/pizza.js", "js");
            $('#pizza').load("/pizza");
            pizza = 0;
            $.ajax({
                url: "/trash",
                data: $('#orders div:first p').val(),
                success: function(result) {
                    let total = parseInt($('#score').text()) - result;
                    $('#score').text(total);
                }
            });
        } else {
            $('#notrash').show();
        }
    });

    $('#createPizza').click(function() {
        // Delete pizza and change score
        if (pizza == 0) {
            pizza = 1;
            $.ajax({
            url: "/make-pizza",
            success: function(result) {
                console.log(result)
                }
            });
        } else{
            $('#already').show();
        }
    });

    $('.topping').click(function (topping) {
        // Add topping value by 1 + show image
        if (pizza == 1) {
            $.ajax({
                type: 'POST',
                url: "/add-topping",
                data: topping.currentTarget.firstChild.data,
                success: function(result) {
                    console.log("Add " + result);
                },
                error: function(error) {
                    console.log(error);
                }
            });
        } else {
            $('#nopizza').show();
        }
    });
});

function removejscssfile(filename, filetype){
    var targetelement=(filetype=="js")? "script" : (filetype=="css")? "link" : "none" //determine element type to create nodelist from
    var targetattr=(filetype=="js")? "src" : (filetype=="css")? "href" : "none" //determine corresponding attribute to test for
    var allsuspects=document.getElementsByTagName(targetelement)
    for (var i=allsuspects.length; i>=0; i--){ //search backwards within nodelist for matching elements to remove
        if (allsuspects[i] && allsuspects[i].getAttribute(targetattr)!=null && allsuspects[i].getAttribute(targetattr).indexOf(filename)!=-1)
            allsuspects[i].parentNode.removeChild(allsuspects[i]) //remove element by calling parentNode.removeChild()
    }
}
